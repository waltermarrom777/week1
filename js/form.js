"use strict";
const registerData = {
  email : "", 
  name : "",
  lastname : "",
  password : "",
  passwordConfirmation : "",
  phone : "",
  age : 0,
  website : "",
  leveExperience : "",
  termAndCondition: false,
  levelEnglish: 0
}

const sendData = function(event){
  event.preventDefault();
  if(validateForm()){
    let levelEnglish = document.getElementById("levelEnglish").value;
    registerData.levelEnglish = levelEnglish;
    console.log("***************** Formulario valido ***********************");
    console.log(`
      email: ${registerData.email}
      name: ${registerData.name}
      lastname: ${registerData.lastname}
      password: ${"*".repeat(registerData.password.length)}
      phone: ${registerData.phone}
      age: ${registerData.age}
      website: ${registerData.website}
      levelExpecience: ${registerData.leveExperience}
      termAndCondition: ${registerData.termAndCondition}
      levelEnglish: ${registerData.levelEnglish}
    `);
  }else{
    console.log("Formulario invalido");
  }
}

const validateForm = function(){
  return isValidEmail() && isValidText("name") && isValidText("lastname") && isValidPassword() && isValidPasswordConfirmation() && isValidPhone() && isValidAge() && isValidWebsite() && isValidLevelExperience() && isValidAcceptTerms();
}

const isValidEmail = function(){
  let email = document.getElementById("email").value;
  let regex = new RegExp("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,4})+$");
  let isValid = regex.test(email);
  removeWarning("emailLabel");
  if (isValid) {
    registerData.email = email;
  }
  else addWarning("emailLabel", "email","Email is not valid!");
  return isValid;
}

const isValidText = function(nameInput){
  removeWarning(`${nameInput}Label`);
  let data = document.getElementById(nameInput).value;
  let isValid = data.length > 2;
  if (isValid){
    registerData[nameInput] = data;
  }else{
    addWarning(`${nameInput}Label`, nameInput ,`The field ${nameInput} must be at least 3 characters!`);
  }
  return isValid;
}

const isValidPassword = function(){
  let password = document.getElementById("password").value;
  let isValid = password.length > 4;
  removeWarning("passwordLabel");
  if (!isValid){
    addWarning("passwordLabel", "password","Passwords must be at least 5 characters!");
  }
  return isValid;
}

const isValidPasswordConfirmation = function(){
  let password = document.getElementById("password").value;
  let passwordConfirmation = document.getElementById("passwordConfirmation").value;
  removeWarning("passwordConfirmationLabel");
  let isValid = password === passwordConfirmation;
  if(isValid){
    registerData.password = password;
    registerData.passwordConfirmation = passwordConfirmation;
  }else{
    addWarning("passwordConfirmationLabel", "passwordConfirmation","Passwords don't match!");
  }
  return isValid;
}

const isValidPhone = function(){
  let phone = document.getElementById("phone").value;
  let regex = new RegExp("^[0-9]{4}[-]{1}[0-9]{4}$");
  let isValid = regex.test(phone);
  removeWarning("phoneLabel");
  if (isValid){
    registerData.phone = phone;
  }else{
    addWarning("phoneLabel","phone" ,"Phone is not valid! must be like xxxx-xxxx");
  }
  return isValid;
}

const isValidAge = function(){
  let age = document.getElementById("age").value;
  let regex = new RegExp("[0-9]*");
  let isValid = regex.test(age);
  removeWarning("ageLabel");
  if(isValid){
    isValid = age > 0;
    if (isValid){
      registerData.age = age;
    }else{
      addWarning("ageLabel", "age", "The age must be major to 0!");
    }
  }else{
    addWarning("ageLabel", "age", "The age must be major to 0 and only numbers!");
  }
  return isValid;
}

const isValidWebsite = function(){
  let regex = new RegExp("[https?://]?[\\w\\-]+(\\.[\\w\\-]{2,})+[/#?]?.*$");
  let website = document.getElementById("website").value;
  let isValid = regex.test(website);
  removeWarning("websiteLabel");
  if(isValid){
    registerData.website = website;
  }else{
    addWarning("websiteLabel", "website", "url is not valid! try again");
  }
  return isValid;
}

const isValidAcceptTerms = function(){
  let termAndCondition = document.getElementById("termAndCondition").checked;
  removeWarning("termAndConditionLabel");
  if (termAndCondition){
    registerData.termAndCondition = termAndCondition;
  }else{
    addWarning("termAndConditionLabel", "termAndCondition", "You must accept the terms and condition!");
  }
  return termAndCondition;
}

const isValidLevelExperience = function(){
  let leveExperience = document.getElementById("leveExperience").value;
  let isValid = leveExperience != "";
  removeWarning("levelExperienceLabel");
  if (isValid){
    registerData.leveExperience = leveExperience;
  }else{
    addWarning("levelExperienceLabel", "leveExperience", "This field cannot be in blank!");
  }
  return isValid;
}


const addWarning = function(idLabel, nameInput, text){
  let node = document.createElement('div');
  node.className='warning';
  node.appendChild(document.createTextNode(text));
  document.getElementById(idLabel).appendChild(node);
  document.getElementById(nameInput).focus();
}

const removeWarning = function(idLabel){
  let nodeParent = document.getElementById(idLabel);
  while(nodeParent.firstChild){
    nodeParent.removeChild(nodeParent.lastChild);
  }
}


document.addEventListener("DOMContentLoaded", ()=>{
  document.getElementById("email").addEventListener("keyup", isValidEmail);
  document.getElementById("name").addEventListener("keyup", ()=>{isValidText("name");});
  document.getElementById("lastname").addEventListener("keyup", ()=>{isValidText("lastname");});
  document.getElementById("password").addEventListener("keyup", isValidPassword);
  document.getElementById("passwordConfirmation").addEventListener("keyup", isValidPasswordConfirmation);
  document.getElementById("phone").addEventListener("keyup", isValidPhone);
  document.getElementById("age").addEventListener("keyup", isValidAge);
  document.getElementById("website").addEventListener("keyup", isValidWebsite);
});

